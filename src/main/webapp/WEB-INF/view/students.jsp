<%@ page import="_6th_Students.Students" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Students</title>
</head>
<body>
<%
    List<Students> student = (List<Students>) request.getAttribute("students");
%>
<table>
    <tr>
        <th>id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Email</th>
        <th>Group id</th>
    </tr>
    <% for(Students g: student) { %>
    <tr>
        <td><%= g.getId() %></td>
        <td><%= g.getFirstName() %></td>
        <td><%= g.getLastName() %></td>
        <td><%= g.getAge() %></td>
        <td><%= g.getEmail() %></td>
        <td><%= g.getGroupId() %></td>
    </tr>
    <% } %>
</table>
</body>
</html>
