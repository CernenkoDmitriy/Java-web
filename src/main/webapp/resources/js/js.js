var counter = 2;
var counterLang = 2;

function Add() {
    var container = document.getElementById("compLanguage");

    var x = document.createElement("INPUT");
    x.setAttribute("type", "text");
    x.setAttribute("name", "computerLanguage" + counter);
    container.appendChild(x);

    var select = document.createElement("SELECT");
    select.setAttribute("name", "skillLvl" + counter);
    container.appendChild(select);

    var option1 = document.createElement("OPTION");
    option1.setAttribute("value", "Beginner");
    option1.text = "Beginner";

    var option2 = document.createElement("OPTION");
    option2.setAttribute("value", "Middle");
    option2.text = "Middle";


    var option3 = document.createElement("OPTION");
    option3.setAttribute("value", "Senior");
    option3.text = "Senior";


    select.add(option1);
    select.add(option2);
    select.add(option3);

    container.appendChild(document.createElement("br"));


    counter++;
}

function Remove() {
    var container = document.getElementById("compLanguage");

    if (counter > 2) {
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        counter--;
    }
}

function AddLang() {
    var container = document.getElementById("lang");

    var x = document.createElement("INPUT");
    x.setAttribute("type", "text");
    x.setAttribute("name", "language" + counterLang);
    container.appendChild(x);

    var comprehension = document.createElement("SELECT");
    comprehension.setAttribute("name", "comprehension" + counterLang);
    container.appendChild(comprehension);
    var reading = document.createElement("SELECT");
    reading.setAttribute("name", "reading" + counterLang);
    container.appendChild(reading);
    var writing = document.createElement("SELECT");
    writing.setAttribute("name", "writing" + counterLang);
    container.appendChild(writing);

    var option1 = document.createElement("OPTION");
    option1.setAttribute("value", "Beginner");
    option1.text = "Beginner";
    var option2 = document.createElement("OPTION");
    option2.setAttribute("value", "Middle");
    option2.text = "Middle";
    var option3 = document.createElement("OPTION");
    option3.setAttribute("value", "Senior");
    option3.text = "Senior";

    var option11 = document.createElement("OPTION");
    option11.setAttribute("value", "Beginner");
    option11.text = "Beginner";
    var option21 = document.createElement("OPTION");
    option21.setAttribute("value", "Middle");
    option21.text = "Middle";
    var option31 = document.createElement("OPTION");
    option31.setAttribute("value", "Senior");
    option31.text = "Senior";

    var option12 = document.createElement("OPTION");
    option12.setAttribute("value", "Beginner");
    option12.text = "Beginner";
    var option22 = document.createElement("OPTION");
    option22.setAttribute("value", "Middle");
    option22.text = "Middle";
    var option32 = document.createElement("OPTION");
    option32.setAttribute("value", "Senior");
    option32.text = "Senior";


    comprehension.add(option1);
    comprehension.add(option2);
    comprehension.add(option3);

    reading.add(option11);
    reading.add(option21);
    reading.add(option31);

    writing.add(option12);
    writing.add(option22);
    writing.add(option32);

    container.appendChild(document.createElement("br"));


    counterLang++;
}

function RemoveLang() {
    var container = document.getElementById("lang");

    if (counterLang > 2) {
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        container.removeChild(container.lastChild);
        counterLang--;
    }
}
