<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cv</title>
    <link rel="stylesheet" href="/resources/css/cv.css">
</head>
<body>

<%! private int colspanComp =1; %>
<% while ( (request.getParameter("computerLanguage"+colspanComp))!=null){colspanComp++;}%>
<%! private int colspanLang =1; %>
<% while ( (request.getParameter("language"+colspanLang))!=null){colspanLang++;}%>

<table>
    <tr><td colspan="2">Personal Information</td></tr>
    <tr><td>First Name</td><td><%= request.getParameter("firstName") %></td></tr>
    <tr><td>Last Name</td><td><%= request.getParameter("lastName") %></td></tr>
    <tr><td>Email</td><td><%= request.getParameter("email") %></td></tr>
    <tr><td>Phone Number</td><td><%= request.getParameter("phoneNumber") %></td></tr>
    <tr><td>Gender</td><td><%= request.getParameter("sex") %></td></tr>
    <tr><td>Birth Date</td><td><%= request.getParameter("birthDate") %></td></tr>
</table>

<table>
    <tr><td colspan="2">Last Work Position</td></tr>
    <tr><td>Company Name</td><td><%= request.getParameter("companyName") %></td></tr>
    <tr><td>From</td><td><%= request.getParameter("from") %></td></tr>
    <tr><td>To</td><td><%= request.getParameter("to") %></td></tr>
</table>

<table>
    <tr><td colspan="3">Computer Skills</td></tr>
    <tr><td rowspan="<%= colspanComp%>">Programming Language</td><td>Language</td><td>Skill Level</td></tr>
    <%
        int counter = 1;
        while ( (request.getParameter("computerLanguage"+counter))!=null){
            out.println(
                    "<tr><td>" + request.getParameter("computerLanguage"+counter) +
                    "</td><td>" + request.getParameter("skillLvl"+counter) + "</td></tr>");
            counter++;
        }

    %>
</table>

<table>
    <tr><td colspan="5">Other Skills</td></tr>
    <tr>
        <td rowspan="<%= colspanLang%>">Languages</td>
        <td>Language</td>
        <td>Comprehension</td>
        <td>Reading</td>
        <td>Writing</td>
    </tr>
    <%
        int counterLang = 1;
        while ( (request.getParameter("language"+counterLang))!=null){
            out.println(
                    "<tr><td>" + request.getParameter("language1") + "</td>" +
                            "<td>" + request.getParameter("comprehension"+counterLang) + "</td>" +
                            "<td>" + request.getParameter("reading"+counterLang) + "</td>" +
                            "<td>" + request.getParameter("writing"+counterLang) + "</td>" +
                            "</tr>");
            counterLang++;
        }

    %>
    <tr><td>Driver's License</td><td colspan="4"><%= request.getParameter("driverLicense1") %>
        <%= request.getParameter("driverLicense2") %>
        <%= request.getParameter("driverLicense3") %></td></tr>
</table>

</body>
</html>
