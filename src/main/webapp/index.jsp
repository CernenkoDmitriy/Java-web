<html>
<head>
    <title>Resume</title>
    <link rel="stylesheet" href="/resources/css/cv.css">
</head>
<body>
<form method='post' action='/Cv.jsp'>
    <fieldset>
        <legend>
            Personal Information
        </legend>
        <input type="text" name="firstName" placeholder="First Name"><br>
        <input type="text" name="lastName" placeholder="Last Name"><br>
        <input type="text" name="email" placeholder="Email"><br>
        <input type="text" name="phoneNumber" placeholder="Phone Number"><br>
        <label>Female<input type="radio" name="sex" value="female" checked/></label>
        <label>Male<input type="radio" name="sex" value="male"></label><br>
        <label>Birth Date<br><input type="text" name="birthDate" placeholder="dd/mm/yyyy">
        </label><br>
        <label>
            Nationally<br>
            <select name="national">
                <option value="Ukrainian">Ukrainian</option>
                <option value="Dutch">Dutch</option>
                <option value="Chines">Chines</option>
            </select>
        </label>
    </fieldset>
    <fieldset>
        <legend>
            Last Work Position
        </legend>
        <label>Company Name<input type="text" name="companyName"></label><br>
        <label>From<input type="text" name="from" placeholder="dd/mm/yyyy"></label><br>
        <label>To<input type="text" name="to" placeholder="dd/mm/yyyy"></label>
    </fieldset>
    <fieldset>
        <legend>
            Computer Skills
        </legend>
        <label>Programming Languages<br></label>
        <div id="compLanguage">
            <input type="text" name="computerLanguage1">
            <select name="skillLvl1">
                <option value="Beginner">Beginner</option>
                <option value="Middle">Middle</option>
                <option value="Senior">Senior</option>
            </select><br>
        </div>
        <input type="button" value="Remove Language" onclick="Remove()">
        <input type="button" value="Add Language" onclick="Add()">
    </fieldset>
    <fieldset>
        <legend>Other Skills</legend>
        Language
        <br>
        <div id="lang">
            <input type="text" name="language1">
            <select name="comprehension1">
                <option selected disabled>-comprehension-</option>
                <option value="Beginner">Beginner</option>
                <option value="Middle">Middle</option>
                <option value="Pro">Pro</option>
            </select>
            <select name="reading1">
                <option selected disabled>-Reading-</option>
                <option value="Beginner">Beginner</option>
                <option value="Middle">Middle</option>
                <option value="Pro">Pro</option>
            </select>
            <select name="writing1">
                <option selected disabled>-Writing-</option>
                <option value="Beginner">Beginner</option>
                <option value="Middle">Middle</option>
                <option value="Pro">Pro</option>
            </select><br>
        </div>
        <input type="button" value="Remove Language" onclick="RemoveLang()">
        <input type="button" value="Add Language" onclick="AddLang()"><br>
        <label>
            Driver's License<br>
            <label>A<input type="checkbox" name="driverLicense1" value="a"></label>
            <label>B<input type="checkbox" name="driverLicense2" value="b"></label>
            <label>C<input type="checkbox" name="driverLicense3" value="c"></label>
        </label>
    </fieldset>
    <input type="submit" value="Generate CV">
</form>

<script src="/resources/js/js.js"></script>
</body>
</html>
