package CW;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class Table extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html");

        int cols;
        int rows;
        if (req.getParameter("cols")!=null) {
            try {
                cols = Integer.valueOf(req.getParameter("cols"));
            }catch (NumberFormatException ex) {
                cols =10;
            }
        }else {
            cols = 10;
        }
        if (req.getParameter("rows")!=null) {
            try {
            rows = Integer.valueOf(req.getParameter("rows"));
            }catch (NumberFormatException ex) {
                rows =10;
            }
        }else {
            rows = 10;
        }

        PrintWriter writer = resp.getWriter();

        writer.write("<html>" +
                "<head>" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"resources/css/css.css\">" +
                "</head>" +
                "<body>" +
                "<div>" +
                "<table>");
        for(int i = 1; i<=rows; i++){
            writer.write("<tr>");
            for (int x = 1; x<=cols; x++){
                if(i==x){
                    writer.write("<td class='centre'>" + x * i + "</td>");
                }else if (x==1 || i==1){
                    writer.write("<td class='first'>" + x * i + "</td>");
                }
                else {
                    writer.write("<td>" + x * i + "</td>");
                }
            }

            writer.write("</tr>");
        }
        writer.write("</table></div></body></html>");
    }
}
