package CW;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CalcServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html");

        PrintWriter writer = resp.getWriter();
        writer.println("<form method='post' style='display:inline;'>" +
                "<input name='a' type='number'/>" +
                "<select name='option'>" +
                "<option>+</option>" +
                "<option>-</option>" +
                "<option>*</option>" +
                "<option>/</option>" +
                "</select>" +
                "<input name='b' type='number'>" +
                "<button type='submit'>=</button>" +
                "</form>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html");

        double a = Double.valueOf(req.getParameter("a"));
        double b = Double.valueOf(req.getParameter("b"));
        String option = req.getParameter("option");
        double result = CalcRes(a,b,option);

        doGet(req,resp);
        resp.getWriter().write("<span>"+result+"</span>");
    }

    private double CalcRes(double a, double b, String option) {

        switch (option) {
            case ("+"):
                return a + b;
            case ("-"):
                return a - b;
            case ("*"):
                return a * b;
            case ("/"):
                return a / b;
            default:
                break;
        }
        return 0;
    }
}
