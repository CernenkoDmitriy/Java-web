package _3rd_4th_JdbsServlet.Database;

import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseComands {

    public static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/academy" +
            "?characterEncoding=UTF-8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC& useSSL=true";
    public static final String USER = "root";
    public static final String PASS = "ghbvf";

    public static final String DROP_STUDENT = "DROP TABLE IF EXISTS student\n";
    public static final String DROP_GROUP = "DROP TABLE IF EXISTS `group`\n";

    public static final String CREATE_STUDENT = "CREATE TABLE IF NOT EXISTS student\n" +
            "(\n" +
            "   id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "   first_name VARCHAR(255) NOT NULL,\n" +
            "   last_name VARCHAR(255) NOT NULL,\n" +
            "   age int,\n" +
            "   email VARCHAR(255) NOT NULL UNIQUE,\n" +
            "   group_id int,\n" +
            "   CONSTRAINT first_last_unique UNIQUE (first_name, last_name)," +
            "   CONSTRAINT student_group_fk FOREIGN KEY (group_id) REFERENCES `group`(id)\n" +
            ")";

    public static final String CREATE_GROUP = "CREATE TABLE IF NOT EXISTS `group`\n" +
            "(\n" +
            "   id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "   name VARCHAR(255) NOT NULL UNIQUE\n" +
            ")\n";

    public static void initDatabase(Statement stmt) throws SQLException {
        stmt.execute(CREATE_STUDENT);
        stmt.execute(CREATE_GROUP);
    }
}
