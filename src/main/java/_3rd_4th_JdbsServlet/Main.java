package _3rd_4th_JdbsServlet;

import _3rd_4th_JdbsServlet.Console.ConsoleEnter;
import _3rd_4th_JdbsServlet.Database.DatabaseComands;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(DatabaseComands.CONNECTION_STRING, DatabaseComands.USER, DatabaseComands.PASS)) {
            Statement stmt = conn.createStatement();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Connected");
            boolean active = true;

            while (active) {
                System.out.println("1 - Добавить группу");
                System.out.println("2 - Показать перечень групп");
                System.out.println("3 - Добавить студента");
                System.out.println("4 - Показать перечень студентов");
                System.out.println("5 - Найти студента");
                System.out.println("6 - Удалить группу");
                System.out.println("7 - Удалить студента");
                System.out.println("8 - Выйти");
                System.out.println("Введите номер команды:");

                String number = scanner.nextLine();
                switch (number) {
                    case "1":
                        ConsoleEnter.AddGroup(stmt);
                        break;
                    case "2":
                        ConsoleEnter.ShowGroups(stmt);
                        break;
                    case "3":
                        ConsoleEnter.AddStudent(stmt);
                        break;
                    case "4":
                        ConsoleEnter.ShowStudents(stmt);
                        break;
                    case "5":
                        ConsoleEnter.GetStudent(stmt);
                        break;
                    case "6":
                        ConsoleEnter.DeleteGroup(stmt);
                        break;
                    case "7":
                        ConsoleEnter.DeleteStudent(stmt);
                        break;
                    case "8":
                        active = false;
                        break;
                    default:
                        System.out.println("Введите корректный номер команды");
                        break;
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
