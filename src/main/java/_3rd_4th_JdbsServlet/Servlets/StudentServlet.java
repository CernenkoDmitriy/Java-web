package _3rd_4th_JdbsServlet.Servlets;

import _3rd_4th_JdbsServlet.Database.DatabaseComands;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class StudentServlet extends HttpServlet {

    private Connection connection;
    private String errorConnection;

    @Override
    public void init() throws ServletException {
        connectDb(); // подключение к б/д
    }

    private void connectDb() {
        try {
            connection = DriverManager.getConnection(DatabaseComands.CONNECTION_STRING, DatabaseComands.USER, DatabaseComands.PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            errorConnection = e.getLocalizedMessage();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");

        PrintWriter out = resp.getWriter();

        out.println("<!doctype html>" +
                "<html>" +
                "   <head>" +
                "       <title>Jdbc and Servlets lesson</title>" +
                "       <meta charset='utf-8'/>" +
                "       <link rel='stylesheet' href='/resources/css/style.css' />" +
                "   </head>" +
                "<body>");

        out.println("<h1>Group</h1>");

        if (connection == null) {
            connectDb();
        }

        if (connection == null) {
            // нет подключения к б/д
            out.println("<p style='color: red'>Error connection: " + errorConnection + "</p>");
        } else {
            // получаем данные с б/д
            try {
                printGroupsTable(out);
                makePost(out);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        out.print("<p><a href='/'>Maine</a></p>");
        out.print("</body>" +
                "</html>");
    }

    private void printGroupsTable(PrintWriter out) throws SQLException {
        if (connection != null && !connection.isClosed()) {
            Statement stmt = connection.createStatement();
            String sql = "SELECT * FROM `student`";
            ResultSet resultSet = stmt.executeQuery(sql);
            out.println("<table>");
            out.println(" <thead>");
            out.println("     <tr>");
            out.println("         <th>Id</th>");
            out.println("         <th>Имя</th>");
            out.println("         <th>Фамилия</th>");
            out.println("         <th>Возвраст</th>");
            out.println("         <th>Имеил</th>");
            out.println("         <th>Ид группы</th>");
            out.println("     </tr>");
            out.println(" </thead>");
            out.println(" <tbody>");
            while (resultSet.next()) {
                out.println(" <tr>");
                out.format("     <td>%d</td>%n", resultSet.getInt("id"));
                out.format("     <td>%s</td>%n", resultSet.getString("first_name"));
                out.format("     <td>%s</td>%n", resultSet.getString("last_name"));
                out.format("     <td>%d</td>%n", resultSet.getInt("age"));
                out.format("     <td>%s</td>%n", resultSet.getString("email"));
                out.format("     <td>%d</td>%n", resultSet.getInt("group_id"));
                out.println(" </tr>");
            }
            out.println(" </tbody>");
            out.println("</table>");
        }
    }

    @Override
    public void destroy() {
        disconnectDb();
    }

    private void disconnectDb() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void makePost(PrintWriter out) throws SQLException {
        Statement stmt = connection.createStatement();
        String sql = "SELECT * FROM `group`";
        ResultSet resultSet = stmt.executeQuery(sql);
        out.println("<form class='addGroup' method='post' action='/students'><fieldset>" +
                "<legend>Добавить студента</legend>" +

                "<div class='wrapper-table'><label for='first_name'>Имя студента:</label>" +
                "<input type='text' id='first_name' name='first_name'/></div>" +

                "<div class='wrapper-table'><label for='last_name'>Фамилия студента:</label>" +
                "<input type='text' id='last_name' name='last_name'/></div>" +

                "<div class='wrapper-table'><label for='age'>Возвраст:</label>" +
                "<input type='text' id='age' name='age'/></div>" +

                "<div class='wrapper-table'><label for='email'>Имеил:</label>" +
                "<input type='text' id='email' name='email'/></div>" +

                "<div class='wrapper-table'><label for='group_id'>ИД группы:</label>" +

                "<select id='group_id' name='group_id'>\n" +
                "<option selected  disabled >Выберите группу</option>");

        while (resultSet.next()) {
            out.println("<option value='" + resultSet.getString("id") + "'>" +
                    resultSet.getString("name") + "</option>\n");
        }

        out.println("</select></div>" +
                "<input class='button-send' type='submit'/></fieldset>" +
                "</form>");


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf8");
        req.setCharacterEncoding("utf8");
        try {
            if (connection != null && !connection.isClosed()) {
                Statement stmt = connection.createStatement();
                String name = req.getParameter("first_name");
                String lastName = req.getParameter("last_name");
                int age = Integer.parseInt(req.getParameter("age"));
                String email = req.getParameter("email");
                int idGroup = Integer.parseInt(req.getParameter("group_id"));

                String sql = String.format("INSERT INTO student(" +
                                "first_name,last_name, age, email, group_id) VALUES('%s','%s','%d','%s','%d')",
                        name, lastName, age, email, idGroup);
                stmt.executeUpdate(sql);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        doGet(req, resp);
    }
}