package _3rd_4th_JdbsServlet.Servlets;

import _3rd_4th_JdbsServlet.Database.DatabaseComands;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

public class GroupServlet extends HttpServlet {

    private Connection connection;
    private String errorConnection;

    @Override
    public void init() throws ServletException {
        connectDb(); // подключение к б/д
    }

    private void connectDb() {
        try {
            connection = DriverManager.getConnection(DatabaseComands.CONNECTION_STRING, DatabaseComands.USER, DatabaseComands.PASS);
        } catch (SQLException e) {
            e.printStackTrace();
            errorConnection = e.getLocalizedMessage();
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf-8");

        PrintWriter out = resp.getWriter();

        out.println("<!doctype html>" +
                "<html>" +
                "   <head>" +
                "       <title>Jdbc and Servlets lesson</title>" +
                "       <meta charset='utf-8'/>" +
                "       <link rel='stylesheet' href='/resources/css/style.css' />" +
                "   </head>" +
                "<body>");

        out.println("<h1>Group list</h1>");

        if (connection == null) {
            connectDb();
        }

        if (connection == null) {
            // нет подключения к б/д
            out.println("<p style='color: red'>Error connection: " + errorConnection + "</p>");
        } else {
            // получаем данные с б/д
            try {
                printGroupsTable(out);
                makePost(out);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        out.print("<p><a href='/'>Maine</a></p>");
        out.print("</body>" +
                "</html>");
    }

    private void printGroupsTable(PrintWriter out) throws SQLException {
        if (connection != null && !connection.isClosed()) {
            Statement stmt = connection.createStatement();
            String sql = "SELECT id, name FROM `group`";
            ResultSet resultSet = stmt.executeQuery(sql);
            out.println("<table>");
            out.println(" <thead>");
            out.println("     <tr>");
            out.println("         <th>Id</th>");
            out.println("         <th>Name</th>");
            out.println("     </tr>");
            out.println(" </thead>");
            out.println(" <tbody>");
            while (resultSet.next()) {
                out.println(" <tr>");
                out.format("     <td>%d</td>%n", resultSet.getInt("id"));
                out.format("     <td>%s</td>%n", resultSet.getString("name"));
                out.println(" </tr>");
            }
            out.println(" </tbody>");
            out.println("</table>");
        }
    }

    @Override
    public void destroy() {
        disconnectDb();
    }

    private void disconnectDb() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void makePost(PrintWriter out){
        out.println("<form class='addGroup' method='post' action='/groups'><fieldset>" +
                "<legend>Добавить группу</legend>" +
                "<label for='group_name'>Название группы:</label>" +
                "<input type='text' id='group_name' name='group_name'/><br/>" +
                "<input class='button-send' type='submit'/></fieldset>" +
                "</form>");


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf8");
        req.setCharacterEncoding("utf8");
        try {
            if (connection != null && !connection.isClosed()) {
                Statement stmt = connection.createStatement();
                if (req.getParameter("group_name")!=null) {
                    String sql = "INSERT INTO academy.`group` (name) VALUES ('" + req.getParameter("group_name") + "');";
                    stmt.execute(sql);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        doGet(req, resp);
    }
}
