package _3rd_4th_JdbsServlet.Console;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ConsoleEnter {


    public static void AddGroup(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите название группы: ");
        String group = scanner.nextLine();
        String sql = String.format("INSERT INTO `group`(name) VALUES('%s')", group);
        stmt.executeUpdate(sql);
    }

    public static void ShowGroups(Statement stmt) throws SQLException {
        String sql = "SELECT * FROM `group`;";
        ResultSet result = stmt.executeQuery(sql);
        System.out.format("id\tGroup name%n");
        while (result.next()) {
            System.out.format("%d\t%s%n", result.getInt(1), result.getString(2));
        }
    }

    public static void AddStudent(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите имя студена: ");
        String name = scanner.nextLine();
        System.out.print("Введите фамилию студена: ");
        String lastName = scanner.nextLine();
        System.out.print("Введите возвраст студена: ");
        int age = Integer.parseInt(scanner.nextLine());
        System.out.print("Введите имеил студена: ");
        String email = scanner.nextLine();
        System.out.print("Введите ид группы студена: ");
        int idGroup = Integer.parseInt(scanner.nextLine());
        String sql = String.format("INSERT INTO student(" +
                "first_name,last_name, age, email, group_id) VALUES('%s','%s','%d','%s','%d')",
                name, lastName, age, email, idGroup);
        stmt.executeUpdate(sql);
    }

    public static void ShowStudents(Statement stmt) throws SQLException {
        String sql = "SELECT * FROM student;";
        ResultSet result = stmt.executeQuery(sql);
        System.out.format("id\tИмя\tФамилия\tВозвраст\tИмеил\tИд группы%n");
        while (result.next()) {
            System.out.format("%d\t%s\t%s\t%d\t%s\t%d%n",
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getInt(4),
                    result.getString(5),
                    result.getInt(6));
        }
    }

    public static void GetStudent(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите имя студена: ");
        String name = scanner.nextLine();
        System.out.print("Введите фамилию студена: ");
        String lastName = scanner.nextLine();

        String sql = String.format("SELECT * FROM student WHERE first_name='%s' AND last_name='%s';", name, lastName);
        ResultSet result = stmt.executeQuery(sql);
        System.out.format("id\tИмя\tФамилия\tВозвраст\tИмеил\tИд группы%n");
        while (result.next()) {
            System.out.format("%d\t%s\t%s\t%d\t%s\t%d%n",
                    result.getInt(1),
                    result.getString(2),
                    result.getString(3),
                    result.getInt(4),
                    result.getString(5),
                    result.getInt(6));
        }
    }

    public static void DeleteStudent(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите студента для удаления: ");
        String name = scanner.nextLine();
        System.out.print("Введите фамилию студена для удаления: ");
        String lastName = scanner.nextLine();

        String sql = String.format("DELETE FROM student WHERE first_name='%s' AND last_name='%s';", name, lastName);
        stmt.execute(sql);
    }

    public static void DeleteGroup(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Название группы для удаления: ");
        String name = scanner.nextLine();

        String sql = String.format("DELETE FROM `group` WHERE name='%s';", name);
        stmt.execute(sql);
    }
}
