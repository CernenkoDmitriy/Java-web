package _2nd_Servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServletForm extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        writer.println("<h1>" + getServletName() + "</h1>");
        writer.println("<form method='post' action='/'>" +
                "<label for='course_name'>Название курса:</label>" +
                "<input type='text' id='course_name' name='course_name'/><br/>" +
                "<label for='course_duration'>Длительность курса, мес.:</label>" +
                "<input type='number' id='course_duration' name='course_duration'/><br/>" +
                "<label for='course_category'>Категория курса:</label>" +
                "<select id='course_category' name='course_category'>" +
                "<option value='Java'>Java</option>" +
                "<option value='Net Framework'>Net Framework</option>" +
                "<option value='Python'>Python</option>" +
                "<option value='Ruby'>Ruby</option>" +
                "<option value='HTML5 & CSS3'>HTML5 & CSS3</option>" +
                "<option value='Node.js'>Node.js</option></select><br/>" +
                "<label for='short_description'>Краткое описание:</label>" +
                "<textarea rows='3' cols='35' id='short_description' name='short_description'/></textarea><br/>" +
                "<input type='submit'/>" +
                "</form>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.setCharacterEncoding("utf8");

        String courseName = req.getParameter("course_name");
        String courseDuration = req.getParameter("course_duration");
        String courseCategory = req.getParameter("course_category");
        String shortDescription = req.getParameter("short_description");

        PrintWriter writer = resp.getWriter();
        writer.println("<h1>" + getServletName() + "</h1>");
        writer.println("<table>" +
                "<tr><td>Название курса</td><td>"+courseName+"</td></tr>"+
                "<tr><td>Длительность курса, мес.</td><td>"+courseDuration+"</td></tr>"+
                "<tr><td>Категория курса</td><td>"+courseCategory+"</td></tr>"+
                "<tr><td>Краткое описание</td><td>"+shortDescription+"</td></tr>"+
                "</table>");
    }
}
